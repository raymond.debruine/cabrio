const request = require('request');
const weather = require('./weather');

var getAddress = (address) => {
  request({
    url: `http://maps.googleapis.com/maps/api/geocode/json?address=${address}`,
    json: true
  }, (error, response, body) => {
    console.log(address);
    if(error) {
      console.log('Error with connection with google', error);
    } else if(body.status === "ZERO_RESULTS"){
      console.log('Address not found');
    } else if(body.status == 'OK') {
      let lat = body.results[0].geometry.location.lat;
      let lng = body.results[0].geometry.location.lng;
      weather.getWeather(lat, lng);
    }
  });
};

module.exports.getAddress = getAddress

