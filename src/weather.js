const request = require('request');

const APIKEY = '0454256d0949a061d2ba1ef87621d89b';

var getWeather = (lat, lng) => {
  request({
    url: `https://api.darksky.net/forecast/${APIKEY}/${lat},${lng}?lang=nl&units=auto`,
    json: true
  }, (error, response, body) => {
    if(error) {
      console.log('Error with connecting darksky', error);
    } else {
      console.log(body.hourly.summary);
      console.log(body.hourly.data[0].temperature);
      console.log(body.daily.summary);
      console.log(body.daily.data[0].temperatureHigh);
    }
  });
}

module.exports.getWeather = getWeather
